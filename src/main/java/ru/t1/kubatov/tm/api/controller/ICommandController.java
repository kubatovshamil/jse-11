package ru.t1.kubatov.tm.api.controller;

public interface ICommandController {

    void showWelcome();

    void showHelp();

    void showVersion();

    void showDeveloperInfo();

    void showCommands();

    void showCommandError();

    void showArguments();

    void showArgumentError();

}
