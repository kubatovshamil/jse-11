package ru.t1.kubatov.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void deleteTaskByID();

    void deleteTaskByIndex();

    void showTaskByID();

    void showTaskByProjectID();

    void showTaskByIndex();

    void updateTaskByID();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

}
