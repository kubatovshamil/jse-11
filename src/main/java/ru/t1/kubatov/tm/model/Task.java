package ru.t1.kubatov.tm.model;

import java.util.Date;
import java.util.UUID;

import ru.t1.kubatov.tm.api.model.IWBS;
import ru.t1.kubatov.tm.enumerated.Status;

public final class Task implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private String projectId;

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public Task() {

    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (description != null && !description.isEmpty()) result += " : " + description;
        if (status != null) result += " : " + Status.toName(status);
        return result;
    }

    @Override
    public Date getCreated() {
        return this.created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }
}
